# 第五次作业

## 分别实现以下三个功能

> 本次作业上传成功1分，全部运行正确一分，至少一个运行正确0.5分
>



###### 1.生成50个div元素，为奇数的div元素显示“hello world!”：偶数的div元素显示“你好世界！”

###### 2.用户输入用户名，并在id为"name"的span标签中显示用户的输入的用户名。

###### 3.有一个超链接(a)标签如下,利用DOM修改访问的超链接为"www.163.com"。

`<a href="www.baidu.com">点我访问</a>`





可能用到的语句：

```
for，%，createElement，createTextNode，appendChild，getElementByid,getElementByTagName,setAttribute，prompt

```

输出形式不限（可选）：

```
alert、document.write、console.log
```

**注意：**

每个函数写在一个HTML文件中，分别命名为HTML1、HTML2、HTML3